import os

def tecla():
    input('Presione "Enter" para continuar...')

def menu():
    os.system('cls')
    print('=============================')
    print('1- Cargar productos')
    print('2- Mostrar lista de productos')
    print('3- Mostrar producto que cuenta con un stock dentro de un intervalo')
    print('4- Sumar los valores de stock menores al maximo valor del intervalo')
    print('5- Eliminar producto sin Stock')
    print('6- Fin')
    print('=============================')
    opcion = int(input('Seleccione una opcion: '))
    while not(opcion >= 1 and opcion <= 6):
        opcion = int(input('Seleccione una opcion: '))
    return opcion

def leerStock():
    stock = int(input('stock: '))
    while not(stock >= 0):
        stock = int(input('stock: '))
    return stock

def leerPrecio():
    precio = float(input('Precio: '))
    while not(precio > 0):
        precio = float(input('Precio: '))
    return precio

def leerDescripcion():
    descripcion = input('Descripcion: ')
    while not(descripcion != ''):
        descripcion = input('Descripcion: ')
    return descripcion
    
def leerProducto():
    productos={}
    codigo = 1
    while codigo!=0:
        codigo = int(input('Ingrese codigo del producto ("0" para finalizar): '))
        if codigo != 0:
            if codigo not in productos: 
                descripcion = leerDescripcion()
                precio = leerPrecio()
                stock = leerStock()
                productos[codigo]=[descripcion,precio,stock]
                print('Producto cargado')
            else:
                print('Producto existente')
    return productos

def mostrar(productos):
    print('Lista de productos')
    print(f'{"Codigo":<10} {"Producto":^15} {"Precio($)":^10} {"Stock":>10}')
    for codigo in productos:
        print(f'{codigo:<10} {productos[codigo][0]:^15} {productos[codigo][1]:^10} {productos[codigo][2]:>10}')

def buscarStock(productos):
    min = int(input('Minimo: '))
    max = int(input('Maximo: '))
    print(f'{"Codigo":<10} {"Producto":^15} {"Precio($)":^10} {"Stock":>10}')
    for codigo in productos:
        if (min <= productos[codigo][2])and(productos[codigo][2] <= max):
           print(f'{codigo:<10} {productos[codigo][0]:^15} {productos[codigo][1]:^10} {productos[codigo][2]:>10}')

def aumentarStock(productos):
    x= int(input('Cuanto aumentar al stock?? '))
    y= int(input('Siempre que el stock sea menor que '))
    for codigo in productos:
        if productos[codigo][2]<y:
            productos[codigo][2]=productos[codigo][2] + x
    print('Stock aumentado...')

def borrarSinStock(productos):
    confir = input('Borrar los productos sin Stock??(s/n) ')
    if confir=='s':
        for codigo in list(productos):
            if productos[codigo][2]==0:
                del productos[codigo]
        print('Eliminado...')
    
#principal
opcion = 0
os.system('cls')
while opcion!=6:
    opcion = menu()
    if opcion == 1:
        productos = leerProducto()
        tecla()
    elif opcion == 2:
        mostrar(productos)
        tecla()
    elif opcion == 3:
        buscarStock(productos)
        tecla()
    elif opcion == 4:
        aumentarStock(productos)
        tecla()
    elif opcion == 5:
        borrarSinStock(productos)
        tecla()
    elif opcion == 6:
        print('Fin :D')